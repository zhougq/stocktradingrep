import requests
import pandas as pd
import os
from datetime import datetime,date

now = date.today()
print(now.strftime("%Y-%m-%d"))
dirname = "/Users/christina/Desktop/JavaDevelopment/CSV/"+now.strftime("%Y")
if not(os.path.isdir(dirname)): os.mkdir(dirname)

url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-chart"
headers = {
    'x-rapidapi-key': "8a3d724b03msh67ddfe934ab186ep1a0c25jsnbd4719eae3fa",
    'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com"
    }

stocklist = "/Users/christina/Desktop/JavaDevelopment/CSV/StockList1.txt"
with open(stocklist) as f:
    for line in f:
        if not line[0].isdigit():
            symbols= line.split(',')
            print(symbols)
            for item in symbols:
                item =item.strip('\n')
                querystring = {"interval":"1m","symbol":item,"range":"1d","region":"US"}
                try:
                    minuteData_jason = requests.request("GET", url, headers=headers, params=querystring).json()
                    for i in range(len(minuteData_jason['chart']['result'][0]['timestamp'])):
                        minuteData_jason['chart']['result'][0]['timestamp'][i] = datetime.fromtimestamp(minuteData_jason['chart']['result'][0]['timestamp'][i])
                    
                    minuteData= {'Datetime': minuteData_jason['chart']['result'][0]['timestamp'], \
                               'Open': minuteData_jason['chart']['result'][0]['indicators']['quote'][0]['open'], \
                               'High':minuteData_jason['chart']['result'][0]['indicators']['quote'][0]['high'], \
                               'Low':minuteData_jason['chart']['result'][0]['indicators']['quote'][0]['low'], \
                               'Close':minuteData_jason['chart']['result'][0]['indicators']['quote'][0]['close'],\
                               'Volume':minuteData_jason['chart']['result'][0]['indicators']['quote'][0]['volume']}
                    minuteData_dataFrame = pd.DataFrame(minuteData)
                    minuteData_dataFrame.set_index('Datetime',inplace= True)
                    fileName = dirname+'/'+ item+'_'+now.strftime("%Y%m") +'.csv'
#                print(fileName)
                    if os.path.exists(fileName):
                        with open(fileName) as s:
                            last_line = s.readlines()[-1]
#                          print(last_line)
                            if (now.strftime("%Y-%m-%d") in last_line):
                                print(last_line)
                            else:   minuteData_dataFrame.to_csv(fileName, mode='a')
                    else: minuteData_dataFrame.to_csv(fileName)
                except (TypeError, NameError,Exception):
                    print('Error in:'+ item)
